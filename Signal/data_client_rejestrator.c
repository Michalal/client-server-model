#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

struct sigaction act;
int signalNumber;
pid_t pid;

void checkArgc(int argc);
void my_detailed_signal_handler(int signum, siginfo_t *info, void *ucontext);

int main(int argc, char **argv) {
    printf("PID: %d\n", getpid());
    checkArgc(argc);
    int opt;
    opterr = 0;
    while ((opt = getopt(argc, argv, "c:")) != -1) {
        if (opt == 'c') {
            char *ptr;
            signalNumber = (int) strtol(optarg, &ptr, 10);
            if (*ptr != '\0') {
                perror("Error signalNumber!");
                exit(1);
            }
        }

    }
    char *ptr;
    pid = (pid_t) strtol(argv[2], &ptr, 10);
    if (*ptr != '\0') {
        perror("Error pid!");
        exit(1);
    }
    act.sa_sigaction = my_detailed_signal_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    sigaction(signalNumber, &act, NULL);

    char buffer[100];
    char *rem;
    while (1) {
        scanf("%s", buffer);
        if (strcmp((buffer), "exit") == 0) {
            break;
        }
        float value = strtof(buffer, &rem);
        if (*rem != '\0') {
            continue;
        }
        union sigval val;
        memcpy(&val.sival_int, &value, sizeof(int));
        int result = sigqueue(pid, signalNumber, val);
        struct timespec ts, remm;
        ts.tv_sec = 2;
        ts.tv_nsec = 0;
        nanosleep(&ts, &remm);
    }
}


void checkArgc(int argc) {
    if (argc < 2) {
        perror("Argc error!");
        exit(1);
    }
}

void my_detailed_signal_handler(int signum, siginfo_t *info, void *ucontext) {
    sigaction(signum, &act, NULL);
    int pid2 = info->si_pid;
    printf("Received signal %d from %d with value %d\n", signum, pid2, info->si_value.sival_int);
}