#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

struct sigaction act;

int signalNumber;
int pid;

void checkArgc(int argc);

void my_detailed_signal_handler(int signum, siginfo_t *info, void *ucontext);

void decodeMessage(int q);

void waitForSignal();

int main(int argc, char **argv) {
    checkArgc(argc);
    int opt;
    opterr = 0;
    while ((opt = getopt(argc, argv, "c:")) != -1) {
        if (opt == 'c') {
            char *ptr;
            signalNumber = strtol(optarg, &ptr, 10);
            if (*ptr != '\0') {
                perror("Error signalNumber!");
                exit(1);
            }
        }
    }
    char *ptr;
    pid = strtol(argv[2], &ptr, 10);
    if (*ptr != '\0') {
        perror("Error pid!");
        exit(1);
    }
    waitForSignal();
}

void waitForSignal() {
    act.sa_sigaction = my_detailed_signal_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    sigaction(signalNumber, &act, NULL);
    union sigval val;
    val.sival_int = 255;
    int result = sigqueue(pid, signalNumber, val);
    if (result < 0) {
        perror("Sigqueue error!");
    }
    struct timespec ts, rem;
    ts.tv_sec = 2;
    ts.tv_nsec = 0;
    if (nanosleep(&ts, &rem) == 0) {
        perror("Signal not received\n");
        exit(0);
    }
}

void checkArgc(int argc) {
    if (argc < 2) {
        perror("Argc error!");
        exit(1);
    }
}

void my_detailed_signal_handler(int signum, siginfo_t *info, void *ucontext) {
    ucontext = NULL;
    sigaction(signum, &act, ucontext);
    int pid2 = info->si_pid;
    printf("Received signal %d from %d with value %d\n", signum, pid2, info->si_value.sival_int);
    int q = info->si_value.sival_int;
    decodeMessage(q);
}

void decodeMessage(int q) {
    if (q >= 8) {
        printf("->uzywany jest format binarny.\n");
        q -= 8;
    }
    if (q >= 4) {
        printf("->uzywana jest identyfikacja zródeł\n");
        q -= 4;
    }
    if (q >= 2) {
        printf("->uzywany jest punkt referencyjny\n");
        q -= 2;
    }
    if (q >= 1) {
        printf("->rejestracja działa\n");
        q -= 1;
    }
}