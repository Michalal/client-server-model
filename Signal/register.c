#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <bits/time.h>
#include <time.h>

char binaryPath[200];
char textPath[200];
int dataSignalNumber;
int commandSignalNumber;

int tfd;
int bfd;
struct sigaction act;
int no_reference_point = 1;     // 1-use CLOCK_REALTIME, 0-use reference_point and CLOCK_MONOTONIC time difference
struct timespec reference_points[1000];
int reference_point_size = 0;
int reference_point_previous_index = 0;
int use_identification_of_sources = 0;
int registration_stopped = 0;

void handleDataSignal(siginfo_t *info);

void handleCommandSignal(int signum, siginfo_t *info);

void my_detailed_signal_handler(int signum, siginfo_t *info, void *ucontext);

void closeFiles();

void checkArgc(int argc);

void writeToBinaryFile(struct timespec ts, float data, pid_t source);

void writeToTextFile(struct timespec ts, float data, pid_t source);

void openTextFile();

void openBinaryFile();

void getDataSignalNumber();

void getCommandSignalNumber();

int checkMask(int mask);

void checkCommands(int sival);

void checkReferencePoint();

int main(int argc, char **argv) {
    checkArgc(argc);
    printf("PID %d\n", getpid());
    int opt;
    opterr = 0;
    while ((opt = getopt(argc, argv, "b:t:d:c:")) != -1) {
        if (opt == 'b') {
            strcpy(binaryPath, optarg);
        } else if (opt == 't') {
            strcpy(textPath, optarg);
        } else if (opt == 'd') {
            getDataSignalNumber();
        } else if (opt == 'c') {
            getCommandSignalNumber();
        }
    }
    openTextFile();
    openBinaryFile();
    atexit(closeFiles);
    act.sa_sigaction = my_detailed_signal_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    sigaction(dataSignalNumber, &act, NULL);
    sigaction(commandSignalNumber, &act, NULL);
    while (1);
}

void getCommandSignalNumber() {
    char *ptr;
    commandSignalNumber = strtol(optarg, &ptr, 10);
    if (*ptr != '\0') {
        perror("Error commandSignalNumber!");
    } else if (commandSignalNumber < SIGRTMIN && SIGRTMAX < commandSignalNumber) {
        perror("Signal with flag -c is not Real-Time signal");
    }
}

void getDataSignalNumber() {
    char *ptr;
    dataSignalNumber = strtol(optarg, &ptr, 10);
    if (*ptr != '\0') {
        perror("Error dataSignalNumber!");
    } else if (dataSignalNumber < SIGRTMIN && SIGRTMAX < dataSignalNumber) {
        perror("Signal with flag -d is not Real-Time signal");
    }
}

void openBinaryFile() {
    bfd = (strcmp(binaryPath, "") != 0) ? open(binaryPath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR) : 1;
    if (bfd == -1) {
        printf("Binary %d %s\n", errno, strerror(errno));
    }
}

void openTextFile() {
    tfd = (strcmp(textPath, "") != 0) ? open(textPath, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR) : 1;
    if (tfd == -1) {
        printf("Text %d %s\n", errno, strerror(errno));
    }
}

void closeFiles() {
    if (tfd > 1) {
        close(tfd);
    }
    if (bfd > 1) {
        close(bfd);
    }
}

void checkArgc(int argc) {
    if (argc < 2) {
        perror("Argc error!");
        exit(1);
    }
}

void my_detailed_signal_handler(int signum, siginfo_t *info, void *ucontext) {
    ucontext = NULL;
    sigaction(signum, &act, ucontext);
    int pid2 = info->si_pid;
    printf("Received signal %d from %d\n", signum, pid2);
    if (signum == dataSignalNumber) {
        handleDataSignal(info);
    } else if (signum == commandSignalNumber) {
        handleCommandSignal(signum, info);
    } else {
        printf("Unexpected signal %d\n", signum);
    }
}

void handleCommandSignal(int signum, siginfo_t *info) {
    int sival = info->si_value.sival_int;
    if (sival == 0) {
        exit(0);
    } else if (sival == 255) {
        int mask = 0;
        mask = checkMask(mask);
        union sigval val;
        val.sival_int = mask;
        printf("pid %d sig %d %d\n", info->si_pid, signum, mask);
        int result = sigqueue(info->si_pid, signum, val);
        if (result == -1) {
            perror("Sending info failed.");
        }
    } else {
        checkCommands(sival);
    }
}

void checkCommands(int sival) {
    if (sival == 1 + 0) {
        no_reference_point = 1;
    } else if (sival == 1 + 1) {
        no_reference_point = 0;
        clock_gettime(CLOCK_MONOTONIC, &reference_points[reference_point_size++]);
        reference_point_previous_index = reference_point_size - 2;

    } else if (sival == 1 + 2) {
        checkReferencePoint();
    } else if (sival == 1 + 4) {
        use_identification_of_sources = 1;
    } else if (sival == 1 + 8) {
        close(tfd);
        tfd = (strcmp(textPath, "") != 0) ? open(textPath, O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR) : 1;
        if (tfd == -1) {
            printf("Text file %d %s\n", errno, strerror(errno));
        }
    }
}

void checkReferencePoint() {
    if (no_reference_point) {
        no_reference_point = 0;
        if (reference_point_size == 0) {
            clock_gettime(CLOCK_MONOTONIC, &reference_points[reference_point_size++]);
        }
        reference_point_previous_index = reference_point_size - 2;
    } else {
        if (reference_point_previous_index >= 0) {
            reference_points[reference_point_size++] = reference_points[reference_point_previous_index--];
        } else {
            clock_gettime(CLOCK_MONOTONIC, &reference_points[reference_point_size++]);
            reference_point_previous_index = reference_point_size - 2;
        }
    }
}

int checkMask(int mask) {
    if (!registration_stopped) {
        mask |= 1;
    }
    if (!no_reference_point) {
        mask |= 2;
    }
    if (!use_identification_of_sources) {
        mask |= 4;
    }
    if (!registration_stopped) {
        mask |= 8;
    }
    return mask;
}

struct timespec getTimeRecord(int abs) {
    if (no_reference_point || abs) {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        return ts;
    } else {
        struct timespec ts, ref = reference_points[reference_point_size - 1], res;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        res.tv_sec = ts.tv_sec - ref.tv_sec;
        res.tv_nsec = ts.tv_nsec - ref.tv_nsec;
        if (res.tv_nsec < 0) {
            res.tv_sec--;
            res.tv_nsec += 1e9;
        }
        return res;
    }
}


void handleDataSignal(siginfo_t *info) {
    pid_t source = info->si_pid;
    struct timespec tst = getTimeRecord(1);
    struct timespec tsb = getTimeRecord(0);
    float data;
    memcpy(&data, &info->si_value.sival_int, sizeof(float));
    writeToBinaryFile(tsb, data, source);
    writeToTextFile(tst, data, source);
}

int absTimespec2str(char *buf, int len, struct timespec *ts) {
    int ret;
    struct tm t;
    if (gmtime_r(&(ts->tv_sec), &t) == NULL) {
        return 1;
    }
    ret = strftime(buf, len, "%T", &t);
    if (ret == 0) {
        return 2;
    }
    len -= ret - 1;
    ret = snprintf(&buf[strlen(buf)], len, ".%09ld", ts->tv_nsec);
    if (ret >= len) {
        return 3;
    }
    return 0;
}

int timespec2str(char *buf, uint len, struct timespec *ts) {
    size_t ret;
    struct tm t;
    if (localtime_r(&(ts->tv_sec), &t) == NULL) {
        return 1;
    }
    ret = strftime(buf, len, "%F %T", &t);
    if (ret == 0) {
        return 2;
    }
    len -= ret - 1;
    ret = (size_t) snprintf(&buf[strlen(buf)], len, ".%09ld", ts->tv_nsec);
    if (ret >= len) {
        return 3;
    }
    return 0;
}

void writeToBinaryFile(struct timespec ts, float data, pid_t source) {
    char buffer[100];
    char helper[30];
    absTimespec2str(helper, 30, &ts);
    sprintf(buffer, "%s %f %d \n", helper, data, source);
    write(bfd, buffer, strlen(buffer));
}

void writeToTextFile(struct timespec ts, float data, pid_t source) {
    char helper[30];
    if (no_reference_point) {
        absTimespec2str(helper, 30, &ts);
    } else {
        timespec2str(helper, 30, &ts);
    }

    char buffer[100];
    if (use_identification_of_sources) {
        sprintf(buffer, "Time=%s Data=%f PID=%d \n", helper, data, source);
    } else {
        sprintf(buffer, "Time=%s Data=%f \n", helper, data);
    }
    write(tfd, buffer, strlen(buffer));
}
