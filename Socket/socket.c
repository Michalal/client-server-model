#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <memory.h>
#include <time.h>

#define MAXLINE 1024

float amp = 1.0;
float freq = 0.25;
float probe = 1.0;
float period = 0;
short pid = 24021;
short rt = 0;

unsigned short portNum;

int sock_fd;
ssize_t len;
int parameters_changed = 0;

void processReceivedMessages(int sock_fd, ssize_t len, char *buffer, struct sockaddr_in *cliaddr);

void init(short rtSig, unsigned short portNumber);


unsigned short getPortNumber(char *portString);

void checkArgc(int argc);

void startSimulation();

void generateSin(int sock_fd);

float getEndSimulation();

int sendSignal(float value);

int initSocket();

int createSocket();

void handleReceive(int sock_fd, ssize_t n, char *buffer, struct sockaddr_in cliaddr);

void parse_buffer(char buffer[1024], char name[7], char val[20]);

void closeSocket();

void changeAmp(const char *val);

void changeFreq(const char *val);

void changeProbe(const char *val);

void changePeriod(const char *val);

void changePid(const char *val);

void changeRt(const char *val);

void sendRaport(int sock_fd, struct sockaddr_in *cliaddr);

void checkPeriod(char *send);

void checkPid(char *send);

void checkRt(char *send);

int main(int argc, char **argv) {
    checkArgc(argc);
    unsigned short portNumber = getPortNumber(argv[1]);
    printf("Port %d \n", portNumber);
    init(37, portNumber);
    startSimulation();
    exit(0);
}

void closeSocket() {
    if (shutdown(sock_fd, SHUT_RDWR)) {
        perror("Shutdown error");
        exit(2);
    }
}

int initSocket() {
    sock_fd = createSocket();
    struct sockaddr_in servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(portNum);
    if (bind(sock_fd, (const struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }
    atexit(closeSocket);
    return sock_fd;
}

int createSocket() {
    sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock_fd < 0) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
    return sock_fd;
}

void checkArgc(int argc) {
    if (argc != 2) {
        perror("Expected one argument -> port number!");
        exit(1);
    }
}

unsigned short getPortNumber(char *portString) {
    char *ptr;
    long number = strtol(portString, &ptr, 10);
    if (*ptr != '\0') {
        perror("Incorrect port value!");
        exit(1);
    }
    return (unsigned short) number;
}

void init(short rtSig, unsigned short portNumber) {
    rt = rtSig;
    portNum = portNumber;
}

void startSimulation() {
    int sock_fd = initSocket();
    while (1) {
        generateSin(sock_fd);
    }
}

float f(float t) {
    float value = amp * sinf(freq * t);
    return value;
}

void generateSin(int sock_fd) {
    float t = 0, dt = 1 / probe, end = getEndSimulation();
    ssize_t len;
    char buffer[MAXLINE];
    struct sockaddr_in cliaddr;
    memset(&cliaddr, 0, sizeof(cliaddr));
    len = sizeof(cliaddr);
    while (t <= end && !parameters_changed) {
        processReceivedMessages(sock_fd, len, buffer, &cliaddr);
        if (parameters_changed) {
            break;
        }
        float value = f(t);
        int send_result = sendSignal(value);
        if (send_result == 0) {
            t += dt;
            struct timespec ts;
            ts.tv_sec = (int) dt;
            ts.tv_nsec = 1ll * (dt - (int) dt) * 1e9;
            nanosleep(&ts, NULL);
        }
    }
    while (!parameters_changed) {
        processReceivedMessages(sock_fd, len, buffer, &cliaddr);
        if (parameters_changed) {
            break;
        }
    }
    parameters_changed = 0;
}


void handleReceive(int sock_fd, ssize_t n, char *buffer, struct sockaddr_in cliaddr) {
    buffer[n] = 0;
    char name[20];
    char val[50];
    parse_buffer(buffer, name, val);
    if (strcmp(name, "amp") == 0) {
        changeAmp(val);
    } else if (strcmp(name, "freq") == 0) {
        changeFreq(val);
    } else if (strcmp(name, "probe") == 0) {
        changeProbe(val);
    } else if (strcmp(name, "period") == 0) {
        changePeriod(val);
    } else if (strcmp(name, "pid") == 0) {
        changePid(val);
    } else if (strcmp(name, "rt") == 0) {
        changeRt(val);
    } else if (strcmp(name, "raport") == 0) {
        sendRaport(sock_fd, &cliaddr);
    }
}

void sendRaport(int sock_fd, struct sockaddr_in *cliaddr) {
    char *send = "Raport\n";
    checkPeriod(send);
    checkPid(send);
    checkRt(send);
    int len1 = sizeof((*cliaddr));
    int y = sendto(sock_fd, (const char *) send, strlen(send), MSG_CONFIRM,
                   (const struct sockaddr *) cliaddr, len1);;
    printf("Message sent %d %d.\n", y, (*cliaddr).sin_port);
}

void checkRt(char *send) {
    if (SIGRTMIN <= rt && rt <= SIGRTMAX)
        strcat(send, "Acceptable Real-Time value");
    else
        strcat(send, "Unacceptable Real-Time value");
}

void checkPid(char *send) {
    int v = kill(pid, 0);
    if (v == 0)
        strcat(send, "Pid exists\n");
    else
        strcat(send, "Pid not exists\n");
}

void processReceivedMessages(int sock_fd, ssize_t len, char *buffer, struct sockaddr_in *cliaddr) {
    for (ssize_t n = 1; n >= 1;) {
        buffer[0] = 0;
        socklen_t helper = len;
        n = recvfrom(sock_fd, (char *) buffer, MAXLINE, MSG_DONTWAIT, (struct sockaddr *) cliaddr, &helper);
        if (n >= 1) {
            handleReceive(sock_fd, n, buffer, (*cliaddr));
        }
    }
}

void checkPeriod(char *send) {
    if (period < 0) {
        strcat(send, "Period: Stopped\n");
    } else if (period == 0) {
        strcat(send, "Period: Non stop\n");
    } else if (period > 0) {
        strcat(send, "Period: It will be suspended\n");
    }
}

void changeRt(const char *val) {
    char *ptr;
    long con = strtol(val, &ptr, 10);
    if (*ptr != '\0') {
        perror("Error rt change!");
    } else {
        rt = (short) con;
        parameters_changed = 1;
    }
}

void changePid(const char *val) {
    char *ptr;
    long con = strtol(val, &ptr, 10);
    if (*ptr != '\0') {
        perror("Error pid change!");
    } else {
        pid = (short) con;
        parameters_changed = 1;
    }
}

void changePeriod(const char *val) {
    char *ptr;
    float con = strtof(val, &ptr);
    if (*ptr != '\0') {
        perror("Error period change!");
    } else {
        period = con;
        parameters_changed = 1;
    }
}

void changeProbe(const char *val) {
    char *ptr;
    float con = strtof(val, &ptr);
    if (*ptr != '\0') {
        perror("Error probe change!");
    } else {
        probe = con;
        parameters_changed = 1;
    }
}

void changeFreq(const char *val) {
    char *ptr;
    float con = strtof(val, &ptr);
    if (*ptr != '\0') {
        perror("Error freq change!");
    } else {
        freq = con;
        parameters_changed = 1;
    }
}

void changeAmp(const char *val) {
    char *ptr;
    float con = strtof(val, &ptr);
    if (*ptr != '\0') {
        perror("Error amp change!");
    } else {
        amp = con;
        parameters_changed = 1;
    }
}

int sendSignal(float value) {
    union sigval val;
    memcpy(&val.sival_int, &value, sizeof(int));
    int result = sigqueue(pid, rt, val);        // 0 - success, -1 failure
    return result;
}

float getEndSimulation() {
    if (period < 0) {
        return -1;
    }
    if (period > 0) {
        return period;
    }
    return INFINITY;
}

void parse_buffer(char *buffer, char *name, char *val) {
    int i = 0;
    for (; buffer[i] != ':' && buffer[i] != ' ' && buffer[i] != '\n' && buffer[i] != 0; i++) {
        name[i] = buffer[i];
    }
    name[i] = 0;
    if (buffer[i] != 0) {
        i++;
    }
    buffer = buffer + i;
    for (i = 0; buffer[i] != '\n' && buffer[i] != 0; i++) {
        val[i] = buffer[i];
    }
    val[i] = 0;
}