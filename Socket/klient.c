#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <memory.h>

pid_t my_pid;
struct sigaction act;
short rt = 37;

void my_detailed_signal_handler(int signum, siginfo_t *info, void *ucontext) {
    sigaction(signum, &act, NULL);
    int pid = info->si_pid;
    printf("Received signal %d from %d\n", signum, pid);
    float value;
    memcpy(&value, &info->si_value.sival_int, sizeof(float));
    printf("Received simulation value %f from %d\n", value, pid);
}

int main() {
    my_pid = getpid();
    printf("My PID is %d\n", my_pid);
    act.sa_sigaction = my_detailed_signal_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO;
    sigaction(rt, &act, NULL);
    while (1);
}