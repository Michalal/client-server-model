#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT     21348
#define MAXLINE 1024

int main() {
    int sockfd;
    char buffer[MAXLINE];
    struct sockaddr_in servaddr;
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr = INADDR_ANY;
    while (1) {
        gets(buffer);
        if (strcmp(buffer, "exit") == 0) {
            break;
        }
        sendto(sockfd, (const char *) buffer, strlen(buffer),
               MSG_CONFIRM, (const struct sockaddr *) &servaddr,
               sizeof(servaddr));
        if (strcmp(buffer, "raport") == 0) {
            int len;
            len = sizeof(servaddr);
            recvfrom(sockfd, (char *) buffer, MAXLINE, MSG_WAITALL,
                         (struct sockaddr *) &servaddr, &len);
            printf("Received message from monochord: %s.\n", buffer);
        }
    }
    close(sockfd);
    return 0;
}